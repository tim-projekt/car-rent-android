package com.cloud.car.service

import com.cloud.car.constants.Constant
import com.cloud.car.models.*
import com.cloud.car.models.user.*
import com.google.gson.GsonBuilder
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


interface ServerApiService {
    @POST("/auth/login")
    fun login(@Body loginUser: LoginUser): Call<AuthToken>

    @POST("/auth/register")
    fun register(@Body user: UserRegistration): Call<Void>


    @GET("cars")
    fun getCars(): Call<List<Car>>

    @POST("cars/{id}/crash")
    fun addCarCrashAlert(@Header("Authorization")token: String, @Path("id") id: Long, @Body carCrashAlerts: CarCrashAlerts): Call<Void>

    @GET("parkings")
    fun getParkings(): Call<List<Parking>>


    @GET("rentings")
    fun getMyRenting(@Header("Authorization")token: String): Call<Renting>

    @POST("rentings")
    fun rentCar(@Header("Authorization")token: String, @Body renting: Renting): Call<Renting>

    @PUT("rentings/{id}")
    fun stopRent(@Header("Authorization")token: String, @Path("id") id: Long, @Body renting: Renting): Call<Void>

    @GET("rentings/lasthistory")
    fun getLastHistory(@Header("Authorization")token: String, @Query("page") page: Int, @Query("pageSize") pageSize: Int): Single<HistoryResponse>

    @GET("rentings/tariffs")
    fun getTariffs(): Call<List<TariffPlan>>


    @GET("reservations")
    fun getMyReservation(@Header("Authorization")token: String): Call<Reservation>

    @POST("reservations")
    fun reserveCar(@Header("Authorization")token: String, @Body reservation: Reservation): Call<Reservation>

    @DELETE("reservations/{id}")
    fun deleteReservation(@Header("Authorization")token: String, @Path("id") id: Long): Call<Void>

    @GET("reservations/{id}")
    fun getReservationById(@Header("Authorization")token: String, @Path("id") id: Long): Call<Reservation>


    @GET("profile")
    fun getProfile(@Header("Authorization")token: String): Call<User>

    @PUT("profile")
    fun putProfile(@Header("Authorization")token: String, @Body user: User): Call<Void>

    @GET("profile/top-up")
    fun topUp(@Header("Authorization")token: String, @Query("amount") amount: Double): Call<MoneyTransfer>

    @POST("users/changePassword")
    fun changePassword(@Header("Authorization")token: String, @Body password: Password): Call<Void>

    companion object Factory {
        fun create(): ServerApiService {
            val gson = GsonBuilder()
                .setLenient()
                .create()

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(Constant.getServerUrl())
            .build()

            return retrofit.create(ServerApiService::class.java)
        }
    }
}