package com.cloud.car.constants

object Constant {
    var mode: Mode = Mode.SERVER

    private var localIp:String = "10.0.2.2"
    private var wifiIp:String = "192.168.0.51"
    private var wifiHome: String = "192.168.1.141"
    private var serverIp:String = "wypzyczalnia-server.herokuapp.com/"

    private var port: String = "8080"
    fun getServerUrl() :String {
        return when(mode) {
            Mode.WIFI -> "http://$wifiIp:$port/"
            Mode.SERVER -> "https://$serverIp"
            Mode.WIFI_HOME -> "http://$wifiHome:$port/"
            else -> "http://$localIp:$port/"
        }
    }
}

enum class Mode{
    LOCAL, WIFI, SERVER, WIFI_HOME
}

object Preference {
    var TOKEN = "token"
    var USERNAME = "username"
    var PASSWORD = "password"
}