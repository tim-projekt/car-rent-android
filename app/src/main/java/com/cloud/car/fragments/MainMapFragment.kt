package com.cloud.car.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.constraint.ConstraintLayout
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.cloud.car.R
import com.cloud.car.models.*
import com.cloud.car.repositories.RentRepository
import com.cloud.car.repositories.RepositoryProvider
import com.cloud.car.utils.MapMarkerUtil
import com.cloud.car.utils.MapMarkerUtil.LOCATION_PERMISSION_REQUEST_CODE
import com.cloud.car.utils.MapMarkerUtil.REFRESH_RENT_DELAY
import com.cloud.car.utils.MapMarkerUtil.REFRESH_RESERVE_DELAY
import com.cloud.car.utils.MapMarkerUtil.RENTING
import com.cloud.car.utils.MapMarkerUtil.RESERVATION
import com.cloud.car.utils.MapMarkerUtil.RESERVATION_TIME
import com.cloud.car.utils.MapMarkerUtil.mLocationPermissionsGranted
import com.cloud.car.utils.TokenUtil
import com.cloud.car.viewmodel.MapViewModel
import com.cloud.car.viewmodel.UserViewModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import kotlinx.android.synthetic.main.fragment_map.view.*
import kotlinx.android.synthetic.main.rent_reviews.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.concurrent.TimeUnit

class MainMapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    private var mMap: GoogleMap? = null
    var lastLocation: LatLng? = null
    private lateinit var mapFragment: SupportMapFragment
    private lateinit var rentInfoLayout: ConstraintLayout
    private lateinit var button: Button
    private lateinit var textActualTime: TextView
    private lateinit var textActualOperation: TextView
    private lateinit var textCurrentPrice: TextView
    private lateinit var reserve: FloatingActionButton
    private lateinit var rent: FloatingActionButton
    private lateinit var reload: FloatingActionButton
    private lateinit var mapViewModel: MapViewModel
    private lateinit var userViewModel: UserViewModel
    private lateinit var tariffsObserver: Observer<List<TariffPlan>>
    private lateinit var carsObserver: Observer<List<Car>>
    private lateinit var parkingsObserver: Observer<List<Parking>>
    private lateinit var reservationObserver: Observer<Reservation>
    private lateinit var rentingObserver: Observer<Renting>
    private var myReservation: Reservation? = null
    private var myRenting: Renting? = null
    private var focusedCar: Car? = null
    private var rentRepository: RentRepository = RepositoryProvider.provideRentRepository()


    private var handler: Handler = Handler()
    private var reserveTask: Runnable = object : Runnable {
        override fun run() {
            val newTimeStr = countReserveTime()
            setActualTime(newTimeStr)
            mapViewModel.loadReservation(myReservation?.id)
            handler.postDelayed(this, REFRESH_RESERVE_DELAY * 1000)
        }
    }
    private var rentTask: Runnable = object : Runnable {
        override fun run() {
            val newTimeStr = countRentTime()
            setActualTime(newTimeStr)
            handler.postDelayed(this, REFRESH_RENT_DELAY * 1000)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mapViewModel.getTariffs().removeObserver(tariffsObserver)
        mapViewModel.getParkings().removeObserver(parkingsObserver)
        mapViewModel.getCars().removeObserver(carsObserver)
        mapViewModel.getReservation().removeObserver(reservationObserver)
        mapViewModel.getRenting().removeObserver(rentingObserver)

        handler.removeCallbacks(reserveTask)
        handler.removeCallbacks(rentTask)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_map, container, false)
        rentInfoLayout = view.findViewById(R.id.rent_info_layout)
        button = view.findViewById(R.id.button)
        textActualTime = view.findViewById(R.id.textActualTime)
        textActualOperation = view.findViewById(R.id.textActualOperation)
        textCurrentPrice = view.findViewById(R.id.textCurrentPrice)

        MapMarkerUtil.getLocationPermission(this.context!!, this)

        view.switchCar.setOnCheckedChangeListener { _, _ -> refreshMarkers() }
        view.switchParking.setOnCheckedChangeListener { _, _ -> refreshMarkers() }

        initButton(inflater, container)
        initRefresh(view)
        initReserveButton(view)
        listeningViewModel()
        return view
    }

    private fun initRefresh(view: View) {
        reload = view.findViewById(R.id.updateMarker)
        reload.setOnClickListener { view ->
            Snackbar.make(view, "Updating...", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
            mapViewModel.fetchFromServer()
        }
    }

    private fun initButton(inflater: LayoutInflater, container: ViewGroup?) {
        button.setOnClickListener { view ->
            when {
                myReservation != null -> {
                    val builder = AlertDialog.Builder(this@MainMapFragment.requireContext())
                    builder.setTitle("Car reservation")
                    builder.setMessage("Do you want to cancel reservation?")

                    builder.setPositiveButton("YES") { _, _ ->
                        cancelReservation()
                    }
                    builder.setNegativeButton("NO") { _, _ ->
                        Snackbar.make(view, "still reserving", Snackbar.LENGTH_SHORT).show()
                    }
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }
                myRenting != null -> {
                    val ratingRentView = inflater.inflate(R.layout.rent_reviews, container, false)
                    val builder = AlertDialog.Builder(this@MainMapFragment.requireContext())
                    builder.setTitle("Stop car renting")
                    builder.setMessage("Do you want to stop current rent?")
                    builder.setView(ratingRentView)

                    builder.setPositiveButton("YES") { _, _ ->
                        confirmStopRental(ratingRentView)
                        changeButtonsRentState(View.INVISIBLE)
                    }
                    builder.setNegativeButton("No") { _, _ ->
                        Snackbar.make(view, "Continue renting", Snackbar.LENGTH_SHORT).show()
                    }

                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }
                else -> button.visibility = View.INVISIBLE
            }
        }
    }

    private fun initReserveButton(view: View) {
        reserve = view.findViewById(R.id.reserveBtn)
        reserve.setOnClickListener { view ->
            val builder = AlertDialog.Builder(this@MainMapFragment.requireContext())
            builder.setTitle("Car reservation")
            builder.setMessage("Do you want to reserve this car?")

            builder.setPositiveButton("YES") { _, _ ->
                reserveFocusedCar()
            }
            builder.setNegativeButton("NO") { _, _ ->
                Snackbar.make(view, "cancelled", Snackbar.LENGTH_SHORT).show()
            }
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }

        rent = view.findViewById(R.id.rentBtn)
        rent.setOnClickListener {
            showStart()

        }
    }

    fun initMap() {
        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun showStart() {
        activity?.supportFragmentManager?.beginTransaction()!!
            .replace(R.id.fragment_container, ScanFragment())
            .disallowAddToBackStack()
            .commit()
        val navView: NavigationView = activity?.findViewById(R.id.nav_view)!!
        navView.menu.getItem(3).isChecked = true
    }

    private fun countReserveTime(): String {
        val t = RESERVATION_TIME * 1000 - (Date().time - myReservation?.startTime!!)
        return String.format(
            "%02d:%02d",
            TimeUnit.MILLISECONDS.toHours(t),
            TimeUnit.MILLISECONDS.toMinutes(t) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(t))
        )
    }

    private fun countRentTime(): String {
        val t = (Date().time - myRenting?.startTime!!)
        val hm = String.format(
            "%02d:%02d",
            TimeUnit.MILLISECONDS.toHours(t),
            TimeUnit.MILLISECONDS.toMinutes(t) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(t))
        )
        countRentTariff()
        return hm
    }

    private fun countRentTariff() {
        val min: Long = (Date().time - myRenting?.startTime!!) / 1000 / 60
        var amount = 0.0
        val tariffs = mapViewModel.getTariffs().value ?: emptyList()
        for (tariff in tariffs) {
            amount += tariff.price!!
            if (min <= tariff.minutes!!) {
                break
            }
        }

        if (tariffs.isEmpty()) {
            textCurrentPrice.text = R.string.noTariff.toString()
        } else {
            textCurrentPrice.text = amount.toString() + " PLN "
        }
    }

    private fun setActualTime(time: String) {
        textActualTime.text = time
    }

    private fun listeningViewModel() {
        activity?.run {
            mapViewModel = ViewModelProviders.of(this).get(MapViewModel::class.java)
            userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

            tariffsObserver = Observer { tariffs ->
                if (tariffs!!.count() != 0) Log.d("tariff", tariffs[0].id.toString())
            }
            mapViewModel.getTariffs().observe(this, tariffsObserver)

            observeCars()
            observeParkings()

            observeReservation()
            observeRenting()
        }
    }

    private fun observeParkings() {
        parkingsObserver = Observer { parkings ->
            if (parkings!!.count() != 0) Log.d("parking", parkings[0].id.toString())
            refreshMarkers()
        }
        mapViewModel.getParkings().observe(this, parkingsObserver)
    }

    private fun observeCars() {
        carsObserver = Observer { cars ->
            if (cars!!.count() != 0) Log.d("car", cars[0].id.toString())
            refreshMarkers()
        }
        mapViewModel.getCars().observe(this, carsObserver)
    }

    private fun observeRenting() {
        rentingObserver = Observer { renting ->
            rentInfoLayout.visibility =
                if (renting != null || myReservation != null) View.VISIBLE else View.INVISIBLE

            if (myRenting != null && renting == null) {
                myRenting = null
                handler.removeCallbacks(rentTask)
                userViewModel.fetchFromServer()
            }
            if (renting != null) {
                myRenting = renting
                startRentingTimer()
            }

            mapViewModel.fetchFromServer()
            refreshMarkers()
        }
        mapViewModel.getRenting().observe(this, rentingObserver)
    }

    private fun observeReservation() {
        reservationObserver = Observer { reservation ->
            rentInfoLayout.visibility = if (reservation != null || myRenting != null) View.VISIBLE else View.INVISIBLE
            if (myReservation != null && reservation == null) {
                myReservation = reservation
                handler.removeCallbacks(reserveTask)
            }
            if (reservation != null) {
                myReservation = reservation
                startListeningActiveReservation()
                changeButtonsRentState(View.VISIBLE)
            } else
                changeButtonsRentState(View.INVISIBLE)

            mapViewModel.fetchFromServer()
            refreshMarkers()
        }
        mapViewModel.getReservation().observe(this, reservationObserver)
    }

    private fun refreshMarkers() {
        val carsToShow: MutableList<Car> = emptyList<Car>().toMutableList()
        carsToShow.addAll(applySwitchFilter(Car::class))
        val parkingsToShow: MutableList<Parking> = emptyList<Parking>().toMutableList()
        parkingsToShow.addAll(applySwitchFilter(Parking::class))
        mMap?.clear()
        mMap?.let {
            fillWithMarkers(carsToShow, it)
            fillWithMarkers(parkingsToShow, it)
            if (myReservation != null) {
                val latLng = LatLng(myReservation?.car!!.latLngX!!, myReservation?.car!!.latLngY!!)
                MapMarkerUtil.putMarker(it, myReservation?.car, RESERVATION, this.context!!)
                MapMarkerUtil.moveCamera(it, latLng, MapMarkerUtil.DEFAULT_ZOOM)
            } else if (myRenting != null) {
                val latLng = LatLng(myRenting?.car!!.latLngX!!, myRenting?.car!!.latLngY!!)
                MapMarkerUtil.putMarker(it, myRenting?.car, RENTING, this.context!!)
                MapMarkerUtil.moveCamera(it, latLng, MapMarkerUtil.DEFAULT_ZOOM)
            }
        }
    }

    private fun <T, C> applySwitchFilter(mapItem: T): List<C> {
        val switch = when (mapItem) {
            Car::class -> view?.switchCar
            Parking::class -> view?.switchParking
            else -> null
        }
        if (mapItem == Car::class && switch?.isChecked == true) {
            return (mapViewModel.getCars().value ?: emptyList<C>()) as List<C>
        } else if (mapItem == Parking::class && switch?.isChecked == true) {
            return (mapViewModel.getParkings().value ?: emptyList<C>()) as List<C>
        }
        return emptyList()
    }

    private fun <T> fillWithMarkers(mlist: List<T>, googleMap: GoogleMap) {
        if (mlist.isNotEmpty()) {
            mlist.forEach {
                MapMarkerUtil.putMarker(googleMap, it, this.context!!)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        mLocationPermissionsGranted = false

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE && grantResults.isNotEmpty()) {
            for (i in grantResults.indices) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionsGranted = false
                    return
                }
            }
            mLocationPermissionsGranted = true
            initMap()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.setOnMarkerClickListener(this)
        googleMap.setOnMapClickListener(this)

        if (mLocationPermissionsGranted) {
            MapMarkerUtil.getDeviceLocation(googleMap, this)

            if (ActivityCompat.checkSelfPermission(this.context!!, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this.context!!,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            googleMap.isMyLocationEnabled = true
            googleMap.uiSettings.isMyLocationButtonEnabled = false
        }

        mMap = googleMap
        refreshMarkers()
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        focusedCar = mapViewModel.getCarById(p0?.tag.toString().toLong()).value
        if (p0!!.snippet == "Car" && myReservation == null && myRenting == null)
            changeButtonsState(View.VISIBLE)
        else
            changeButtonsState(View.INVISIBLE)
        return false
    }

    override fun onMapClick(p0: LatLng?) {
        focusedCar = null
        changeButtonsState(View.INVISIBLE)
    }

    @SuppressLint("RestrictedApi")
    private fun changeButtonsState(state: Int) {
        view?.reserveBtn?.visibility = state
    }
    @SuppressLint("RestrictedApi")
    private fun changeButtonsRentState(state: Int) {
        view?.rentBtn?.visibility = state
    }

    private fun reserveFocusedCar() {
        val session = TokenUtil.getToken(this.activity?.applicationContext!!)
        val reservation = Reservation(car = focusedCar)
        val call = rentRepository.reserveCar(session!!, reservation)
        call.enqueue(object : Callback<Reservation> {
            override fun onResponse(call: Call<Reservation>?, response: Response<Reservation>?) {
                if (response!!.isSuccessful) {
                    Snackbar.make(
                        this@MainMapFragment.view!!,
                        "Ok, car reserved for 15 min.",
                        Snackbar.LENGTH_SHORT
                    ).show()
                    changeButtonsState(View.INVISIBLE)
                    this@MainMapFragment.mapViewModel.setReservation(response.body())
                    this@MainMapFragment.mapViewModel.fetchFromServer()
                    changeButtonsRentState(View.VISIBLE)
                } else {
                    mapViewModel.fetchFromServer()
                    Snackbar.make(
                        this@MainMapFragment.view!!,
                        "error: " + response.code().toString(),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<Reservation>?, t: Throwable?) {
                mapViewModel.fetchFromServer()
                Snackbar.make(this@MainMapFragment.view!!, "Error...", Snackbar.LENGTH_LONG).show()
            }
        })
    }

    private fun cancelReservation() {
        val session = TokenUtil.getToken(this.activity?.applicationContext!!)
        val id = myReservation?.id

        val call = rentRepository.deleteReservation(session!!, id!!)
        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                if (response!!.isSuccessful) {
                    Snackbar.make(
                        this@MainMapFragment.view!!,
                        "Reservation cancelled",
                        Snackbar.LENGTH_SHORT
                    ).show()
                    mapViewModel.setReservation(null)
                    changeButtonsRentState(View.INVISIBLE)
                }
            }

            override fun onFailure(call: Call<Void>?, t: Throwable?) {
                mapViewModel.fetchFromServer()
                Snackbar.make(this@MainMapFragment.view!!, "error cancelling reservation", Snackbar.LENGTH_LONG).show()
            }
        })
    }

    private fun confirmStopRental(view: View) {
        myRenting!!.stars = view.rent_rating.rating.toDouble()
        myRenting!!.comment = view.rent_comment.editText!!.text.toString()
        stopRenting()
    }

    private fun stopRenting() {
        val session = TokenUtil.getToken(this.activity?.applicationContext!!)
        val renting = Renting(
            id = myRenting?.id,
            stars = myRenting?.stars,
            comment = myRenting?.comment,
            car = myRenting?.car
        )
        val call = rentRepository.stopRent(session!!, renting.id!!, renting)
        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                if (response!!.isSuccessful) {
                    Snackbar.make(
                        this@MainMapFragment.view!!,
                        "Rent ${myRenting?.id} ended! You gave it ${myRenting?.stars} stars",
                        Snackbar.LENGTH_SHORT
                    ).show()
                    mapViewModel.setRenting(null)
                }
            }

            override fun onFailure(call: Call<Void>?, t: Throwable?) {
                mapViewModel.fetchFromServer()
                Snackbar.make(this@MainMapFragment.view!!, "error cancelling reservation", Snackbar.LENGTH_LONG).show()
            }
        })
    }

    private fun startListeningActiveReservation() {
        textActualOperation.text = getString(R.string.reservation)
        textActualOperation.setTextColor(Color.GREEN)
        button.text = getString(R.string.stop_reservation)
        button.setTextColor(Color.BLACK)
        textCurrentPrice.text = ""
        val newTimeStr = countReserveTime()
        setActualTime(newTimeStr)
        handler.postDelayed(reserveTask, REFRESH_RESERVE_DELAY * 1000)
    }

    private fun startRentingTimer() {
        textActualOperation.text = getString(R.string.renting)
        textActualOperation.setTextColor(Color.GREEN)
        button.text = getString(R.string.stop_renting)
        button.setTextColor(Color.RED)
        textCurrentPrice.text = getString(R.string._0_currency)
        if (myReservation != null) mapViewModel.loadReservation(myReservation?.id)
        val newTimeStr = countRentTime()
        setActualTime(newTimeStr)
        handler.postDelayed(rentTask, REFRESH_RENT_DELAY * 1000)
    }
}