package com.cloud.car.fragments

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.cloud.car.R
import com.cloud.car.ScanActivity
import com.cloud.car.models.Car
import com.cloud.car.models.CarCrashAlerts
import com.cloud.car.repositories.MapRepository
import com.cloud.car.repositories.RepositoryProvider
import com.cloud.car.utils.TokenUtil
import com.cloud.car.viewmodel.MapViewModel
import kotlinx.android.synthetic.main.fragment_car_crash.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CarCrashFragment : Fragment() {
    private var fragmentCrashScan: Button? = null
    private var fragmentCrashSubmit: Button? = null
    private lateinit var mapViewModel: MapViewModel
    private var mapRepository: MapRepository = RepositoryProvider.provideMapRepository()
    private var carNum: EditText? = null
    private val crashRequestCode = 37274

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_car_crash, container, false)
        activity?.run {
            mapViewModel = ViewModelProviders.of(this).get(MapViewModel::class.java)
        }

        carNum = view.findViewById(R.id.crashCarNum)
        fragmentCrashScan = view.findViewById(R.id.fragmentCrashScan) as Button
        fragmentCrashSubmit = view.findViewById(R.id.fragmentCrashSubmit) as Button

        fragmentCrashScan!!.setOnClickListener {
            val intent = Intent(this@CarCrashFragment.context, ScanActivity::class.java)
            startActivityForResult(intent, crashRequestCode)
        }
        fragmentCrashSubmit!!.setOnClickListener {
            createNewAlert()
            mapViewModel.fetchFromServer()
        }
        return view
    }

    private fun createNewAlert() {
        if (crashCarNum?.text.isNullOrEmpty() || crashCarNum?.text.isNullOrBlank()) return
        val token = TokenUtil.getToken(this.activity?.applicationContext!!)
        val car = Car(id = crashCarNum?.text.toString().toLong())
        val desc = if (crashDesText.text.isEmpty()) null else crashDesText.text.toString()
        val alert = CarCrashAlerts(car = car, description = desc)

        sendAlert(token!!, alert)
    }

    private fun sendAlert(token: String, alert: CarCrashAlerts){
        val call = mapRepository.addCarCrashAlert(token, alert.car.id!!, alert)
        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                if (response!!.isSuccessful) {
                    Snackbar.make(this@CarCrashFragment.view!!, "Operation SUCCESSFUL", Snackbar.LENGTH_SHORT).show()
                } else {
                    Snackbar.make(
                        this@CarCrashFragment.view!!,
                        "error: " + response.code().toString(),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                showMap()
            }

            override fun onFailure(call: Call<Void>?, t: Throwable?) {
                Snackbar.make(this@CarCrashFragment.view!!, "Error...", Snackbar.LENGTH_LONG).show()
                showMap()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            carNum?.setText(data?.getStringExtra("carQR"))
        }
    }

    private fun showMap() {
        activity?.supportFragmentManager?.beginTransaction()!!
            .replace(R.id.fragment_container, MainMapFragment())
            .disallowAddToBackStack()
            .commit()
        val navigationView: NavigationView = activity?.findViewById(R.id.nav_view)!!
        navigationView.menu.getItem(0).isChecked = true
    }
}
