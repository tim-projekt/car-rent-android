package com.cloud.car.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cloud.car.R
import com.cloud.car.TopUpActivity
import com.cloud.car.models.user.*
import com.cloud.car.repositories.RepositoryProvider
import com.cloud.car.repositories.UserRepository
import com.cloud.car.utils.TokenUtil
import com.cloud.car.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.content_profile.*
import kotlinx.android.synthetic.main.content_profile.view.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileFragment : Fragment() {
    private lateinit var userViewModel: UserViewModel
    private lateinit var user: User
    private var userRepository: UserRepository = RepositoryProvider.provideUserRepository()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listeningViewModel()
    }

    override fun onResume() {
        super.onResume()
        userViewModel.fetchFromServer()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)

        view?.topUp?.setOnClickListener {
            val intent = Intent(this@ProfileFragment.context, TopUpActivity::class.java)
            startActivity(intent)
        }

        initProfile(view)
        initPassword(view)
        initAddress(view)

        return view
    }

    private fun initAddress(view: View) {
        view.profileToggleButtonAddress?.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                saveAddress.visibility = View.VISIBLE
                changeAddressEditText(true)
            } else {
                saveAddress.visibility = View.INVISIBLE
                changeAddressEditText(false)
            }
        }
        view.saveAddress?.setOnClickListener {
            profileToggleButtonAddress.isChecked = !profileToggleButtonAddress.isChecked
            saveProfileAddress()
        }
    }

    private fun initPassword(view: View) {
        view.profileToggleButtonPassword?.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                savePass.visibility = View.VISIBLE
                changePasswordEditText(true)
            } else {
                savePass.visibility = View.INVISIBLE
                changePasswordEditText(false)
            }
        }
        view.savePass?.setOnClickListener {
            profileToggleButtonPassword.isChecked = !profileToggleButtonPassword.isChecked
            saveProfilePassword()
        }
    }

    private fun initProfile(view: View) {
        view.profileToggleButtonInfo?.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                saveInfo.visibility = View.VISIBLE
                changeProfileEditText(true)
            } else {
                saveInfo.visibility = View.INVISIBLE
                changeProfileEditText(false)
            }
        }
        view.saveInfo?.setOnClickListener {
            profileToggleButtonInfo.isChecked = !profileToggleButtonInfo.isChecked
            saveProfileInfo()
        }

    }

    private fun changeProfileEditText(boolean: Boolean) {
        profile_name_surname.isEnabled = boolean
        profile_email.isEnabled = boolean
        profile_card.isEnabled = boolean
    }

    private fun changePasswordEditText(boolean: Boolean) {
        password_old.isEnabled = boolean
        password_new.isEnabled = boolean
        password_confirm.isEnabled = boolean
    }

    private fun changeAddressEditText(boolean: Boolean) {
        address_country.isEnabled = boolean
        address_region.isEnabled = boolean
        address_city.isEnabled = boolean
        address_street.isEnabled = boolean
        address_postal.isEnabled = boolean
    }

    private fun saveProfileInfo() {
        val user = User(
            name = profile_name_surname.text.toString().substringBefore(" "),
            surname = profile_name_surname.text.toString().substringAfter(" "),
            cardNumber = profile_card.text.toString().toLongOrNull(),
            email = profile_email.text.toString(),
            phone = profile_phone.text.toString()
        )
        sendRequest(user)
    }

    private fun saveProfilePassword() {
        val userPassword = Password(
            password = password_old.text.toString(),
            newPassword = password_new.text.toString()
        )
        if (userPassword.newPassword == password_confirm.text.toString()) {
            sendPasswordRequest(userPassword)
        } else {
            Snackbar.make(this@ProfileFragment.view!!, "password not match", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    private fun saveProfileAddress() {
        val country = Country(address_country.text.toString())
        val region = Region(
            address_region.text.toString(),
            country
        )
        val city = City(
            address_city.text.toString(),
            region
        )
        val address = Address(
            address_street.text.toString(),
            address_postal.text.toString(),
            city
        )
        val user = User(
            name = profile_name_surname.text.toString().substringBefore(" "),
            surname = profile_name_surname.text.toString().substringAfter(" "),
            address = address,
            email = profile_email.text.toString(),
            phone = profile_phone.text.toString()
        )
        sendRequest(user)
    }

    private fun sendPasswordRequest(password: Password) {
        val session = TokenUtil.getToken(this.activity?.applicationContext!!)
        val call = userRepository.changeUserPassword(session!!, password)
        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                if (response!!.isSuccessful) {
                    userViewModel.fetchFromServer()
                    Snackbar.make(this@ProfileFragment.view!!, "Profile was updated !", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                } else {
                    userViewModel.fetchFromServer()
                    Snackbar.make(
                        this@ProfileFragment.view!!,
                        "Error in update..." + response.code(),
                        Snackbar.LENGTH_LONG
                    )
                        .setAction("Action", null).show()
                }
            }

            override fun onFailure(call: Call<Void>?, t: Throwable?) {
                userViewModel.fetchFromServer()
                Snackbar.make(this@ProfileFragment.view!!, "Error...", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            }
        })
    }

    private fun sendRequest(user: User) {
        val session = TokenUtil.getToken(this.activity?.applicationContext!!)
        val call = userRepository.updateProfile(session!!, user)
        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                if (response!!.isSuccessful) {
                    userViewModel.fetchFromServer()
                    Snackbar.make(this@ProfileFragment.view!!, "Profile was updated !", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                } else {
                    userViewModel.fetchFromServer()
                    Snackbar.make(this@ProfileFragment.view!!, "Error in update...", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }
            }

            override fun onFailure(call: Call<Void>?, t: Throwable?) {
                userViewModel.fetchFromServer()
                Snackbar.make(this@ProfileFragment.view!!, "Error...", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            }
        })
    }

    private fun listeningViewModel() {
        userViewModel = ViewModelProviders.of(this.activity!!).get(UserViewModel::class.java)
        userViewModel.getProfile().observe(this, Observer { user ->
            this.user = user!!
            renderData()
        })
    }

    private fun renderData() {
        profile_name_surname.setText("${user.name} ${user.surname}")
        profile_balance.text = "balance: ${user.accountBalance} PLN"
        profile_email.setText(user.email)
        profile_phone.setText(user.phone)
        profile_card.setText(user.cardNumber?.toString())
        address_country.setText(user.address?.city?.region?.country?.name)
        address_region.setText(user.address?.city?.region?.name)
        address_city.setText(user.address?.city?.name)
        address_street.setText(user.address?.street)
        address_postal.setText(user.address?.postal)
    }
}
