package com.cloud.car.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

import com.cloud.car.R
import com.cloud.car.models.enums.State
import com.cloud.car.utils.adapters.HistoryListAdapter
import com.cloud.car.viewmodel.HistoryListViewModel
import kotlinx.android.synthetic.main.fragment_history.view.*

class HistoryFragment : Fragment() {
    private lateinit var viewModel: HistoryListViewModel
    private lateinit var historyListAdapter: HistoryListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_history, container, false)
        viewModel = ViewModelProviders.of(this)
            .get(HistoryListViewModel::class.java)
        initAdapter(view)
        initState(view)
        return view
    }

    private fun initAdapter(view: View) {
        historyListAdapter = HistoryListAdapter { viewModel.retry() }
        view.recycler_view.layoutManager = LinearLayoutManager(this.context, LinearLayout.VERTICAL, false)
        view.recycler_view.adapter = historyListAdapter
        viewModel.historyList.observe(this, Observer {
            historyListAdapter.submitList(it)
        })
    }

    private fun initState(view: View) {
        view.txt_error.setOnClickListener { viewModel.retry() }
        viewModel.getState().observe(this, Observer { state ->
            view.history_progress_bar.visibility =
                if (viewModel.listIsEmpty() && state == State.LOADING) View.VISIBLE else View.GONE
            view.txt_error.visibility = if (viewModel.listIsEmpty() && state == State.ERROR) View.VISIBLE else View.GONE
            if (!viewModel.listIsEmpty()) {
                historyListAdapter.setState(state ?: State.DONE)
            }
        })
    }

}
