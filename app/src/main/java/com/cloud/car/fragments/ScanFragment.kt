package com.cloud.car.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import com.cloud.car.R
import com.cloud.car.ScanActivity
import com.cloud.car.models.Car
import com.cloud.car.models.Renting
import com.cloud.car.models.Reservation
import com.cloud.car.repositories.RentRepository
import com.cloud.car.repositories.RepositoryProvider
import com.cloud.car.utils.TokenUtil
import com.cloud.car.viewmodel.MapViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ScanFragment : Fragment() {
    private var startScan: Button? = null
    private var startOK: Button? = null
    private lateinit var mapViewModel: MapViewModel
    private var rentRepository: RentRepository = RepositoryProvider.provideRentRepository()
    private var carNum: EditText? = null
    private val rentRequestCode = 7368

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_scan, container, false)
        activity?.run {
            mapViewModel = ViewModelProviders.of(this).get(MapViewModel::class.java)
        }
        carNum = view.findViewById(R.id.startCarNum) as EditText
        startScan = view.findViewById(R.id.startScan) as Button
        startOK = view.findViewById(R.id.startOK) as Button

        startScan!!.setOnClickListener {
            val intent = Intent(this@ScanFragment.context, ScanActivity::class.java)
            intent.putExtra("RequestCode", rentRequestCode)
            startActivityForResult(intent, rentRequestCode)
        }
        startOK!!.setOnClickListener {
            createNewRenting()
        }

        return view
    }

    private fun createNewRenting() {
        if (carNum?.text.isNullOrEmpty() || carNum?.text.isNullOrBlank()) return
        val session = TokenUtil.getToken(this.activity?.applicationContext!!)
        val car = Car(id = carNum?.text.toString().toLong())
        val newRent = Renting(car = car)

        checkActiveReservation(session!!, newRent)
    }

    private fun checkActiveReservation(session: String, newRent: Renting) {
        val token = TokenUtil.token
        val call = rentRepository.getMyReservation(token!!)
        call.enqueue(object : Callback<Reservation> {
            override fun onResponse(call: Call<Reservation>?, response: Response<Reservation>?) {
                if (response!!.code() == 200) {
                    cancelReservation(response.body()?.id!!)
                    sendRentingRequest(session, newRent)
                } else if (response!!.code() == 404)
                    sendRentingRequest(session, newRent)
            }

            override fun onFailure(call: Call<Reservation>?, t: Throwable?) {
                Snackbar.make(
                    this@ScanFragment.view!!,"response error", Snackbar.LENGTH_LONG
                ).show()
            }
        })
    }

    private fun cancelReservation(id: Long) {
        val session = TokenUtil.getToken(this.activity?.applicationContext!!)
        val call = rentRepository.deleteReservation(session!!, id)
        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                if (response!!.isSuccessful) {
                    Log.d("cancelReservation: ", "ok")
                }
            }

            override fun onFailure(call: Call<Void>?, t: Throwable?) {
                Snackbar.make(
                    this@ScanFragment.view!!, "cancel reservation error", Snackbar.LENGTH_LONG
                ).show()
            }
        })
    }


    private fun sendRentingRequest(session: String, newRent: Renting) {
        val call = rentRepository.rentCar(session, newRent)
        call.enqueue(object : Callback<Renting> {
            override fun onResponse(call: Call<Renting>?, response: Response<Renting>?) {
                if (response!!.isSuccessful) {
                    Snackbar.make(this@ScanFragment.view!!, "Operation SUCCESSFUL", Snackbar.LENGTH_SHORT).show()
                    mapViewModel.setRenting(response.body())
                    mapViewModel.fetchFromServer()
                    showMap()
                } else {
                    Snackbar.make(
                        this@ScanFragment.view!!,
                        "Add funds!",
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<Renting>?, t: Throwable?) {
                Snackbar.make(this@ScanFragment.view!!, "Error...", Snackbar.LENGTH_LONG).show()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            carNum?.setText(data?.getStringExtra("carQR"))
        }
    }

    private fun showMap() {
        activity?.supportFragmentManager?.beginTransaction()!!
            .replace(R.id.fragment_container, MainMapFragment())
            .disallowAddToBackStack()
            .commit()
        val navigationView: NavigationView = activity?.findViewById(R.id.nav_view)!!
        navigationView.menu.getItem(0).isChecked = true
        hideKeyboard(view!!)
    }

    private fun hideKeyboard(view: View) {
        val `in` = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }
}
