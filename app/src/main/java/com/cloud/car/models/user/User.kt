package com.cloud.car.models.user

import com.cloud.car.models.enums.UserStatus

data class User(
    val name: String? = null,
    val surname: String? = null,
    val address: Address? = null,
    val cardNumber: Long? = null,
    val email: String? = null,
    val phone: String? = null,
    val password: String? = null,
    val newPassword: String? = null,
    val confirmPassword: String? = null,
    val photo: String? = null,
    val accountBalance: Double? = null,
    val status: UserStatus? = null,
    val stars: Int? = null
)