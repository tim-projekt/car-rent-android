package com.cloud.car.models.user

data class Region(
    val name: String,
    val country: Country
)