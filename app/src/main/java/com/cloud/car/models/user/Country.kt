package com.cloud.car.models.user

data class Country(
    val name: String
)