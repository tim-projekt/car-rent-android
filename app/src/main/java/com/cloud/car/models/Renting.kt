package com.cloud.car.models

data class Renting(
    val id: Long? = null,
    val car: Car? = null,
    val totalPrice: Double? = null,
    val startLatLngX: Double? = null,
    val startLatLngY: Double? = null,
    val endLatLngX: Double? = null,
    val endLatLngY: Double? = null,
    val startTime: Long? = null,
    val endTime: Long? = null,
    var stars: Double? = null,
    var comment: String? = null
)