package com.cloud.car.models.enums

enum class SimpleStatus {
    ACTIVE, DISACTIVE, ARCHIVED
}