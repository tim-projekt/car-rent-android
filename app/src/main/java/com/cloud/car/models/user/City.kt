package com.cloud.car.models.user

data class City(
    val name: String,
    val region: Region
)