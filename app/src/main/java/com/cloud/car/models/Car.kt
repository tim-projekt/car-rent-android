package com.cloud.car.models

import com.cloud.car.models.enums.CarStatus

data class Car(
    val id: Long?,
    val model: String? = null,
    val status: CarStatus? = null,
    val registrationNumber: String? = null,
    val latLngX: Double? = null,
    val latLngY: Double? = null
)