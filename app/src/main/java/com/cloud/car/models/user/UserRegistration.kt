package com.cloud.car.models.user

data class UserRegistration(
    val confirmPassword:	String? = null,
    val email:	String? = null,
    val name:	String? = null,
    val newPassword:	String? = null,
    val password:	String? = null,
    val phone:	String? = null,
    val surname:	String? = null
)