package com.cloud.car.models

data class HistoryResponse(
    val totalResults: Int,
    val historyList: List<Renting>
)