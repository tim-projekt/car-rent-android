package com.cloud.car.models.user

import com.cloud.car.models.enums.PaymentStatus

data class MoneyTransfer(
    val amount: Double,
    val time: Long,
    val status: PaymentStatus
)