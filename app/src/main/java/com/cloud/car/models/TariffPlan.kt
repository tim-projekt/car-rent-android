package com.cloud.car.models

data class TariffPlan(
    val id: Long? = null,
    val name: String?=null,
    val minutes: Long?=null,
    val price: Double?=null,
    val status:String?=null,
    val type:String?=null
)