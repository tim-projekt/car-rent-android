package com.cloud.car.models

data class CarCrashAlerts(
    val car: Car,
    val description: String?=null
)