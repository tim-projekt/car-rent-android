package com.cloud.car.models.enums

enum class PaymentStatus {
    CREATED, CANCELED, DENIED, RESERVED, PENDING, COMPLETED, LOCAL_TRANSFER
}