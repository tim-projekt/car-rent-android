package com.cloud.car.models

import com.cloud.car.models.enums.SimpleStatus

data class Parking(
    val id: Long,
    val latLngX: Double,
    val latLngY: Double,
    val name: String,
    val parkingSpace: Int,
    val status: SimpleStatus,
    val freeSpace: Int
)