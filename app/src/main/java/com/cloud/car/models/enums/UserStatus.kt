package com.cloud.car.models.enums

enum class UserStatus {
    ACTIVE, DISACTIVE, BANED
}