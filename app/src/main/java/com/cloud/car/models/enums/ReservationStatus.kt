package com.cloud.car.models.enums

enum class ReservationStatus {
    ACTIVE, SUCCESSFUL, CANCELED
}