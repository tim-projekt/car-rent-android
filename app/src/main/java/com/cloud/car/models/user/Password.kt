package com.cloud.car.models.user

data class Password(
    val newPassword: String? = null,
    val password: String? = null
)