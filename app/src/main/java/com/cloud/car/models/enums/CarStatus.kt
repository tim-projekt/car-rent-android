package com.cloud.car.models.enums

enum class CarStatus {
    IN_RENT, ACTIVE, DISACTIVE, BREAK, IN_SERVICE
}