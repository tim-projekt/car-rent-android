package com.cloud.car.models.user

data class Address(
    val street: String,
    val postal: String,
    val city: City
)