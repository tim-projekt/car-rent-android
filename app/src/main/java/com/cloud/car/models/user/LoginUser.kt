package com.cloud.car.models.user

data class LoginUser (
    val phone: String,
    val password: String
)