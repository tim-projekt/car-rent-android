package com.cloud.car.models.enums

enum class State {
    DONE, LOADING, ERROR
}