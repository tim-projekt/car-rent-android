package com.cloud.car.models.user

import java.util.*

data class AuthToken (
    val token: String,
    val expirationTime: Date
)