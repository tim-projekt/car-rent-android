package com.cloud.car.models

import com.cloud.car.models.enums.ReservationStatus
import com.cloud.car.models.user.User

data class Reservation(
    val id: Long? = null,
    val car: Car? = null,
    val user: User? = null,
    val status: ReservationStatus? = null,
    val startTime: Long? = null,
    val endTime: Long? = null
)