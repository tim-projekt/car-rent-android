package com.cloud.car.room

import android.arch.persistence.room.*

@Dao
interface TokenDao {
    @Query("SELECT * FROM token WHERE id = 1")
    fun getToken(): Token

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveToken(vararg token: Token)

    @Query("DELETE FROM token where id =1")
    fun removeToken()

    @Delete
    fun deleteToken(token: Token)
}