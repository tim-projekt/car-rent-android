package com.cloud.car.room

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Token(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "timestamp") val tokenTimestamp: Long?,
    @ColumnInfo(name = "token_value") val tokenValue: String?
)