package com.cloud.car.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.cloud.car.models.Renting
import com.cloud.car.models.enums.State
import com.cloud.car.repositories.RentRepository
import com.cloud.car.repositories.RepositoryProvider
import com.cloud.car.utils.data_sources.HistoryDataSource
import com.cloud.car.utils.data_sources.HistoryDataSourceFactory
import io.reactivex.disposables.CompositeDisposable

class HistoryListViewModel : ViewModel() {

    private var rentRepository: RentRepository = RepositoryProvider.provideRentRepository()
    var historyList: LiveData<PagedList<Renting>>
    private val compositeDisposable = CompositeDisposable()
    private val pageSize = 10
    private val historyDataSourceFactory: HistoryDataSourceFactory

    init {
        historyDataSourceFactory =
            HistoryDataSourceFactory(compositeDisposable, rentRepository)
        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize)
            .setEnablePlaceholders(false)
            .build()

        historyList = LivePagedListBuilder(historyDataSourceFactory, config).build()
    }

    fun getState(): LiveData<State> = Transformations.switchMap<HistoryDataSource, State>(
        historyDataSourceFactory.historyDataSourceLiveData,
        HistoryDataSource::state
    )

    fun retry() {
        historyDataSourceFactory.historyDataSourceLiveData.value?.retry()
    }

    fun listIsEmpty(): Boolean {
        return historyList.value?.isEmpty() ?: true
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}