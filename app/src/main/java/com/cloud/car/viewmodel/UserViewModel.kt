package com.cloud.car.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.cloud.car.models.user.User
import com.cloud.car.repositories.RepositoryProvider
import com.cloud.car.repositories.UserRepository
import com.cloud.car.utils.TokenUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserViewModel : ViewModel() {
    private lateinit var user: MutableLiveData<User>
    private var userRepository: UserRepository = RepositoryProvider.provideUserRepository()

    fun getProfile(): LiveData<User> {
        if (!::user.isInitialized) {
            user = MutableLiveData()
            loadUser()
        }
        return user
    }

    fun fetchFromServer() {
        loadUser()
    }

    private fun loadUser() {
        val token = TokenUtil.token
        val call = userRepository.getProfile(token!!)

        call.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>?, response: Response<User>?) {
                if (response!!.isSuccessful) {
                    user.value = response.body()
                } else {
                    user.value = null
                }
            }

            override fun onFailure(call: Call<User>?, t: Throwable?) {
                user.value = null
            }
        })
    }
}