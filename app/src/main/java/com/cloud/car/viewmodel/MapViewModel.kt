package com.cloud.car.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.cloud.car.models.*
import com.cloud.car.models.enums.ReservationStatus
import com.cloud.car.repositories.MapRepository
import com.cloud.car.repositories.RentRepository
import com.cloud.car.repositories.RepositoryProvider
import com.cloud.car.utils.TokenUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MapViewModel : ViewModel() {

    private lateinit var tariffs: MutableLiveData<List<TariffPlan>>
    private lateinit var cars: MutableLiveData<List<Car>>
    private lateinit var parkings: MutableLiveData<List<Parking>>
    private lateinit var myReservation: MutableLiveData<Reservation>
    private lateinit var myRenting: MutableLiveData<Renting>
    private var mapRepository: MapRepository = RepositoryProvider.provideMapRepository()
    private var rentRepository: RentRepository = RepositoryProvider.provideRentRepository()


    private fun loadTariffs() {
        val call = rentRepository.getTariffs()
        call.enqueue(object : Callback<List<TariffPlan>> {
            override fun onResponse(call: Call<List<TariffPlan>>?, response: Response<List<TariffPlan>>?) {
                if (response!!.isSuccessful) {
                    tariffs.value = response.body() ?: emptyList()
                } else {
                    tariffs.value = emptyList()
                }
            }

            override fun onFailure(call: Call<List<TariffPlan>>?, t: Throwable?) {
                cars.value = emptyList()
            }
        })
    }

    private fun loadCars() {
        val call = mapRepository.getCars()
        call.enqueue(object : Callback<List<Car>> {
            override fun onResponse(call: Call<List<Car>>?, response: Response<List<Car>>?) {
                if (response!!.isSuccessful) {
                    cars.value = response.body() ?: emptyList()
                } else {
                    cars.value = emptyList()
                }
            }

            override fun onFailure(call: Call<List<Car>>?, t: Throwable?) {
                cars.value = emptyList()
            }
        })
    }

    private fun loadParkings() {
        val call = mapRepository.getParkings()
        call.enqueue(object : Callback<List<Parking>> {
            override fun onResponse(
                call: Call<List<Parking>>?,
                response: Response<List<Parking>>?
            ) {
                if (response!!.isSuccessful) {
                    parkings.value = response.body() ?: emptyList()
                } else {
                    parkings.value = emptyList()
                }
            }

            override fun onFailure(call: Call<List<Parking>>?, t: Throwable?) {
                parkings.value = emptyList()
            }
        })
    }

    private fun checkActiveReservation() {
        val token = TokenUtil.token
        val call = rentRepository.getMyReservation(token!!)
        call.enqueue(object : Callback<Reservation> {
            override fun onResponse(call: Call<Reservation>?, response: Response<Reservation>?) {
                if (response!!.isSuccessful) {
                    myReservation.value = response.body()

                } else {
                    myReservation.value = null
                }
            }

            override fun onFailure(call: Call<Reservation>?, t: Throwable?) {
                myReservation.value = null
            }
        })
    }

    private fun checkActiveRenting() {
        val token = TokenUtil.token
        val call = rentRepository.getMyRenting(token!!)
        call.enqueue(object : Callback<Renting> {
            override fun onResponse(call: Call<Renting>?, response: Response<Renting>?) {
                if (response!!.isSuccessful) {
                    myRenting.value = response.body()
                } else {
                    myRenting.value = null
                }
            }

            override fun onFailure(call: Call<Renting>?, t: Throwable?) {
                myRenting.value = null
            }
        })
    }


    fun fetchFromServer() {
        loadCars()
        loadParkings()
    }

    fun getTariffs(): LiveData<List<TariffPlan>> {
        if (!::tariffs.isInitialized) {
            tariffs = MutableLiveData()
            loadTariffs()
        }
        return tariffs
    }

    fun getCars(): LiveData<List<Car>> {
        if (!::cars.isInitialized) {
            cars = MutableLiveData()
            loadCars()
        }
        return cars
    }

    fun getParkings(): LiveData<List<Parking>> {
        if (!::parkings.isInitialized) {
            parkings = MutableLiveData()
            loadParkings()
        }
        return parkings
    }

    fun getCarById(id: Long): LiveData<Car> {
        val list = getCars().value
        val carResponse = MutableLiveData<Car>()
        carResponse.value = list?.find { it -> it.id == id }
        return carResponse
    }

    fun setReservation(reservation: Reservation?) {
        myReservation.value = reservation
    }

    fun getReservation(): LiveData<Reservation> {
        if (!::myReservation.isInitialized) {
            myReservation = MutableLiveData()
            checkActiveReservation()
        }
        return myReservation
    }

    fun loadReservation(id: Long?) {
        val token = TokenUtil.token
        val call = rentRepository.getReservationById(token!!, id!!)
        call.enqueue(object : Callback<Reservation> {
            override fun onResponse(call: Call<Reservation>?, response: Response<Reservation>?) {
                if (response!!.isSuccessful) {
                    if (response.body()?.status != ReservationStatus.ACTIVE)
                        myReservation.value = null
                } else {
                    myReservation.value = null
                }
            }

            override fun onFailure(call: Call<Reservation>?, t: Throwable?) {
                myReservation.value = null
            }
        })
    }

    fun setRenting(renting: Renting?) {
        myRenting.value = renting
    }

    fun getRenting(): LiveData<Renting> {
        if (!::myRenting.isInitialized) {
            myRenting = MutableLiveData()
            checkActiveRenting()
        }
        return myRenting
    }

}