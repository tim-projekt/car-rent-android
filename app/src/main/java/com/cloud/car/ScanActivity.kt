package com.cloud.car

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.cloud.car.models.Car
import com.google.gson.Gson
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView


class ScanActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {
    private var mScannerView: ZXingScannerView? = null
    private var gson: Gson = Gson()

    public override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        mScannerView = ZXingScannerView(this)
        setContentView(mScannerView)
    }

    public override fun onResume() {
        super.onResume()
        mScannerView!!.setResultHandler(this)
        mScannerView!!.startCamera()
    }

    public override fun onPause() {
        super.onPause()
        mScannerView!!.stopCamera()
    }

    override fun handleResult(rawResult: Result) {
        val regex = Regex("""\{"id":"\d+"\}""")
        if (rawResult.text.matches(regex)) {
            val car: Car = gson.fromJson(rawResult.text, Car::class.java)
            intent.putExtra("carQR", car.id.toString())

            setResult(Activity.RESULT_OK, intent)
            finish()
        } else {
            Toast.makeText(this, "wrong QR code", Toast.LENGTH_SHORT).show()
            setResult(Activity.RESULT_CANCELED, intent)
            finish()
        }
    }
}