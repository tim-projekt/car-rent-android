package com.cloud.car.utils.view_holders

import android.location.Geocoder
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cloud.car.R
import com.cloud.car.models.Renting
import kotlinx.android.synthetic.main.list_item_history.view.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class HistoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(renting: Renting?) {
        try {
            if (renting != null) {
                itemView.historyTextRide.text = "Ride ID: " + renting.id
                itemView.historyTextCar.text = renting.car?.model
                itemView.historyTextDate.text = getDate(renting.startTime!!)
                itemView.historyStartTime.text = getTime(renting.startTime)
                itemView.historyEndTime.text = getTime(renting.endTime!!)
                val t = renting.endTime - renting.startTime
                val hm = String.format(
                    "%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(t),
                    TimeUnit.MILLISECONDS.toMinutes(t) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(t))
                )
                itemView.historyTextTime.text = "Time: " + hm
                itemView.historyTextPrice.text = "${renting.totalPrice}zl"
                itemView.historyTextStartLoc.text = getStreetName(renting.startLatLngX!!, renting.startLatLngY!!)
                itemView.historyTextEndLoc.text = getStreetName(renting.endLatLngX!!, renting.endLatLngY!!)
                itemView.historyTextStars.text = "Stars: ${renting.stars.toString()}"
                itemView.historyTextRegNum.text = renting.car?.registrationNumber.toString()
            }
        } catch (e: Exception ) { }
    }

    private fun getStreetName(lat: Double,long: Double): String? {
        val geocoder = Geocoder(this.itemView.context, Locale.getDefault())
        val address = geocoder.getFromLocation(lat,long,1).first()
        return address.getAddressLine(0)
    }

    private fun getDate(timestamp: Long): String? {
        return try {
            val sdf = SimpleDateFormat("dd-MM-yyyy")
            val netDate = Date(timestamp)
            sdf.format(netDate)
        } catch (e: Exception) {
            e.toString()
        }
    }

    private fun getTime(timestamp: Long): String? {
        return try {
            val sdf = SimpleDateFormat("HH:mm")
            val netDate = Date(timestamp)
            sdf.format(netDate)
        } catch (e: Exception) {
            e.toString()
        }
    }

    companion object {
        fun create(parent: ViewGroup): HistoryViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_history, parent, false)
            return HistoryViewHolder(view)
        }
    }
}
