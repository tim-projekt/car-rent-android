package com.cloud.car.utils

import android.Manifest
import android.content.ContentValues.TAG
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import com.cloud.car.R
import com.cloud.car.fragments.MainMapFragment
import com.cloud.car.models.Car
import com.cloud.car.models.Parking
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*


object MapMarkerUtil {
    var RESERVATION = "RESERVATION"
    var RENTING = "RENTING"
    var REFRESH_RESERVE_DELAY: Long = 60
    var REFRESH_RENT_DELAY: Long = 60
    var RESERVATION_TIME: Long = 900

    private const val FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
    private const val COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION
    const val LOCATION_PERMISSION_REQUEST_CODE = 1234
    const val DEFAULT_ZOOM = 15f

    var mLocationPermissionsGranted = false

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap =
            Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    fun <T> putMarker(googleMap: GoogleMap, it: T, context: Context): Marker {
        val marker: Marker
        when (it) {
            is Car -> {
                marker = googleMap.addMarker(
                    MarkerOptions()
                        .position(LatLng(it.latLngX!!, it.latLngY!!))
                        .title(it.model!! + " id:" + it.id)
                        .icon(setMarkerIcon(it, context))
                )
                marker.tag = it.id
                marker.snippet = "Car"
                return marker
            }
            is Parking -> {
                marker = googleMap.addMarker(
                    MarkerOptions()
                        .position(LatLng(it.latLngX, it.latLngY))
                        .title(it.name + " id:" + it.id)
                        .icon(setMarkerIcon(it, context))
                )
                marker.tag = it.id
                marker.snippet = "Parking"
                return marker
            }
        }
        return googleMap.addMarker(MarkerOptions())
    }

    fun <T> putMarker(googleMap: GoogleMap, it: T, markerImage: String, context: Context): Marker {
        val marker: Marker
        when (it) {
            is Car -> {
                marker = googleMap.addMarker(
                    MarkerOptions()
                        .position(LatLng(it.latLngX!!, it.latLngY!!))
                        .title(it.model!! + " id:" + it.id)
                        .icon(setMarkerIcon(markerImage, context))
                )
                marker.tag = it.id
                marker.snippet = "Car"
                return marker
            }

        }
        return googleMap.addMarker(MarkerOptions())
    }

    private fun <T> setMarkerIcon(type: T, context: Context): BitmapDescriptor {
        return when (type) {
            is Car -> bitmapDescriptorFromVector(
                context,
                R.mipmap.ic_marker_car
            )
            is Parking -> bitmapDescriptorFromVector(
                context,
                R.mipmap.ic_marker_parking
            )
            else -> BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)
        }
    }

    private fun setMarkerIcon(markerImage: String, context: Context): BitmapDescriptor {
        return when (markerImage) {
            RESERVATION -> bitmapDescriptorFromVector(
                context,
                R.mipmap.ic_marker_car_reserve
            )
            RENTING -> bitmapDescriptorFromVector(
                context,
                R.mipmap.ic_marker_car_rent
            )
            else -> BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)
        }
    }

    fun moveCamera(googleMap: GoogleMap, latLng: LatLng, zoom: Float) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
    }

    fun getLocationPermission(context: Context, fragment: MainMapFragment) {
        val permissions =
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)

        if (ContextCompat.checkSelfPermission(context, FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(context, COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionsGranted = true
                fragment.initMap()
            } else {
                ActivityCompat.requestPermissions(
                    fragment.activity?.parent!!,
                    permissions,
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        }
    }

    fun getDeviceLocation(googleMap: GoogleMap, fragment: MainMapFragment) {
        val mFusedLocationProviderClient: FusedLocationProviderClient? =
            LocationServices.getFusedLocationProviderClient(fragment.activity!!)

        try {
            if (mLocationPermissionsGranted) {
                val location = mFusedLocationProviderClient?.lastLocation
                location?.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val currentLocation = task.result
                        fragment.lastLocation = LatLng(currentLocation!!.latitude, currentLocation.longitude)
                        moveCamera(
                            googleMap,
                            fragment.lastLocation!!,
                            DEFAULT_ZOOM
                        )
                    } else Log.d(TAG, "current location is null")
                }
            }
        } catch (e: SecurityException) {
            Log.e(TAG, "getDeviceLocation: SecurityException: " + e.message)
        }
    }

}