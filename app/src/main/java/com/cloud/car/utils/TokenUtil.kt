package com.cloud.car.utils

import android.content.Context
import com.cloud.car.room.AppDatabase
import com.cloud.car.room.Token

object TokenUtil {
    var token: String? = null
    lateinit var db: AppDatabase

    fun setToken(context: Context, token: String) {
        this.token = token
        db = AppDatabase.getInstance(context)!!
        db.tokenDao().saveToken(Token(1, System.currentTimeMillis(), token))
    }

    fun getToken(context: Context): String? {
        db = AppDatabase.getInstance(context)!!
        val dbToken = db.tokenDao().getToken()
        token = dbToken?.tokenValue
        return token
    }

    fun logout(context: Context) {
        db = AppDatabase.getInstance(context)!!
        db.tokenDao().removeToken()
        token = null
    }

}