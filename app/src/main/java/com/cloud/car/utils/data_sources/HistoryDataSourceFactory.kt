package com.cloud.car.utils.data_sources

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.cloud.car.models.Renting
import com.cloud.car.repositories.RentRepository
import io.reactivex.disposables.CompositeDisposable

class HistoryDataSourceFactory (
    private val compositeDisposable: CompositeDisposable,
    private val rentRepository: RentRepository
)
    : DataSource.Factory<Int, Renting>() {

    val historyDataSourceLiveData = MutableLiveData<HistoryDataSource>()

    override fun create(): DataSource<Int, Renting> {
        val historyDataSource = HistoryDataSource(rentRepository, compositeDisposable)
        historyDataSourceLiveData.postValue(historyDataSource)
        return historyDataSource
    }
}