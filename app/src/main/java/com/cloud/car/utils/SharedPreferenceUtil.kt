package com.cloud.car.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.cloud.car.constants.Preference.PASSWORD
import com.cloud.car.constants.Preference.USERNAME

object SharedPreferenceUtil {
    private fun getPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun setLoginData(context: Context, username: String, password: String, save: Boolean) {
        val editor = getPreferences(context).edit()
        if(save) {
            editor.putString(USERNAME, username)
            editor.putString(PASSWORD, password)
        } else {
            editor.remove(USERNAME).remove(PASSWORD)
        }
        editor.apply()
    }

    fun getLoginData(context: Context): String {
        return getPreferences(context).getString(USERNAME, "")!!+":"+
                getPreferences(context).getString(PASSWORD, "")
    }
}