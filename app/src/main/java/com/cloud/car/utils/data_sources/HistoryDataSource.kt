package com.cloud.car.utils.data_sources

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import com.cloud.car.models.Renting
import com.cloud.car.models.enums.State
import com.cloud.car.repositories.RentRepository
import com.cloud.car.utils.TokenUtil
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers

class HistoryDataSource(private val rentRepository: RentRepository,private val compositeDisposable: CompositeDisposable)
    : PageKeyedDataSource<Int, Renting>() {

    var state: MutableLiveData<State> = MutableLiveData()
    private var retryCompletable: Completable? = null
    val token = TokenUtil.token

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Renting>) {
        updateState(State.LOADING)
        compositeDisposable.add(

            rentRepository.getLastHistory(token!!,1, params.requestedLoadSize)
                .subscribe(
                    { response ->
                        updateState(State.DONE)
                        callback.onResult(response.historyList, null,2)
                    },
                    {
                        updateState(State.ERROR)
                        setRetry(Action { loadInitial(params, callback) })
                    }
                )
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Renting>) {
        updateState(State.LOADING)
        compositeDisposable.add(
            rentRepository.getLastHistory(token!!,params.key, params.requestedLoadSize)
                .subscribe(
                    { response ->
                        updateState(State.DONE)
                        callback.onResult(response.historyList, params.key +1)
                    },
                    {
                        updateState(State.ERROR)
                        setRetry(Action { loadAfter(params, callback) })
                    }
                )
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Renting>) {
    }

    private fun updateState(state: State) {
        this.state.postValue(state)
    }

    fun retry() {
        if (retryCompletable != null) {
            compositeDisposable.add(retryCompletable!!
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe())
        }
    }

    private fun setRetry(action: Action?) {
        retryCompletable = if (action == null) null else Completable.fromAction(action)
    }

}