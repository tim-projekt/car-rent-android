package com.cloud.car.repositories

import com.cloud.car.models.*
import com.cloud.car.models.user.*
import com.cloud.car.service.ServerApiService
import io.reactivex.Single
import retrofit2.Call

class MapRepository(private val apiService: ServerApiService) {
    fun getCars(): Call<List<Car>> {
        return apiService.getCars()
    }
    fun addCarCrashAlert(token: String, id: Long, carCrashAlerts: CarCrashAlerts): Call<Void> {
        return apiService.addCarCrashAlert("Bearer $token", id, carCrashAlerts)
    }
    fun getParkings(): Call<List<Parking>> {
        return apiService.getParkings()
    }
}

class AuthRepository(private val apiService: ServerApiService) {
    fun login(loginUser: LoginUser): Call<AuthToken> {
        return apiService.login(loginUser)
    }
    fun register(user: UserRegistration): Call<Void> {
        return apiService.register(user)
    }
}

class UserRepository(private val apiService: ServerApiService) {
    fun getProfile(token: String): Call<User> {
        return apiService.getProfile("Bearer $token")
    }
    fun updateProfile(token: String, user: User): Call<Void> {
        return apiService.putProfile("Bearer $token", user)
    }
    fun topUp(token: String, amount: Double): Call<MoneyTransfer> {
        return apiService.topUp("Bearer $token", amount)
    }
    fun changeUserPassword(token: String, password: Password): Call<Void> {
        return apiService.changePassword("Bearer $token", password)
    }
}

class RentRepository(private val apiService: ServerApiService) {
    fun getLastHistory(token: String, page: Int, pageSize: Int): Single<HistoryResponse> {
        return apiService.getLastHistory("Bearer $token", page, pageSize)
    }
    fun getTariffs(): Call<List<TariffPlan>> {
        return apiService.getTariffs()
    }
    fun getMyReservation(token: String): Call<Reservation> {
        return apiService.getMyReservation("Bearer $token")
    }
    fun getReservationById(token: String, id: Long): Call<Reservation> {
        return apiService.getReservationById("Bearer $token", id)
    }
    fun reserveCar(token: String, reservation: Reservation): Call<Reservation> {
        return apiService.reserveCar("Bearer $token", reservation)
    }
    fun deleteReservation(token: String, id: Long): Call<Void> {
        return apiService.deleteReservation("Bearer $token", id)
    }
    fun getMyRenting(token: String): Call<Renting> {
        return apiService.getMyRenting("Bearer $token")
    }

    fun rentCar(token: String, renting: Renting): Call<Renting> {
        return apiService.rentCar("Bearer $token", renting)
    }
    fun stopRent(token: String, id:Long, renting: Renting): Call<Void> {
        return apiService.stopRent("Bearer $token", id, renting)
    }

}