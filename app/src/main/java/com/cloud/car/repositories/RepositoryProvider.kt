package com.cloud.car.repositories

import com.cloud.car.service.ServerApiService

object RepositoryProvider {
    private val apiService = ServerApiService.create()

    fun provideMapRepository(): MapRepository {
        return MapRepository(apiService)
    }

    fun provideAuthRepository(): AuthRepository {
        return AuthRepository(apiService)
    }

    fun provideUserRepository(): UserRepository {
        return UserRepository(apiService)
    }

    fun provideRentRepository(): RentRepository {
        return RentRepository(apiService)
    }

}