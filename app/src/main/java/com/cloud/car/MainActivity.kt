package com.cloud.car

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.cloud.car.fragments.*
import com.cloud.car.models.user.User
import com.cloud.car.utils.TokenUtil
import com.cloud.car.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var userViewModel: UserViewModel
    private lateinit var userObserver: Observer<User>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar_main)
        listeningViewModel()

        showFragment(MainMapFragment())

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar_main, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        userViewModel.getProfile().removeObserver(userObserver)
    }

    private fun listeningViewModel() {
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        userViewModel.getProfile()
        userObserver = Observer { user ->
            nav_bar_name?.text = "${user?.name} ${user?.surname}"
            nav_bar_balance?.text = "${user?.accountBalance} PLN"
        }
        userViewModel.getProfile().observe(this, userObserver)
    }

    private fun showFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .commit()
        drawer_layout.closeDrawer(GravityCompat.START)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            val fragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
            if (fragment != null) {
                if (fragment is MainMapFragment) {
                    super.onBackPressed()
                } else {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, MainMapFragment())
                        .commit()
                }
            } else
                super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_start -> {
                showFragment(ScanFragment())
            }
            R.id.nav_maps -> {
                showFragment(MainMapFragment())
            }
            R.id.nav_history -> {
                showFragment(HistoryFragment())
            }
            R.id.nav_profile -> {
                showFragment(ProfileFragment())
            }
            R.id.nav_crash_car -> {
                showFragment(CarCrashFragment())
            }
            R.id.nav_logout -> {
                TokenUtil.logout(applicationContext)
                val intent = Intent(this@MainActivity, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
        return true
    }

}
