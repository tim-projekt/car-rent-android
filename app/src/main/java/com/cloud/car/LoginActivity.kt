package com.cloud.car

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import com.cloud.car.models.user.LoginUser
import com.cloud.car.models.user.User
import com.cloud.car.models.user.UserRegistration
import com.cloud.car.repositories.RepositoryProvider
import com.cloud.car.utils.SharedPreferenceUtil
import com.cloud.car.utils.TokenUtil
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException


class LoginActivity : AppCompatActivity() {
    private var mAuthTask: UserLoginTask? = null
    private var mRegisterTask: UserRegisterTask? = null
    private val authRepository = RepositoryProvider.provideAuthRepository()
    private val userRepository = RepositoryProvider.provideUserRepository()
    private var loginFormIsActive: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        tryLogIn(TokenUtil.getToken(applicationContext))

        formInit()
        password.setOnEditorActionListener(TextView.OnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })

        initButtons()
    }

    private fun initButtons() {
        primary_button.setOnClickListener {
            if (loginFormIsActive) {
                attemptLogin()
            } else {
                attemptRegister()
            }
        }
        secondary_button.setOnClickListener {
            if (loginFormIsActive) {
                primary_button.text = getString(R.string.register_now)
                secondary_button.text = getString(R.string.i_have_account)
                showRegisterFields(View.VISIBLE)
                title_login.text = getString(R.string.register_new_account)
                clearInputs()
                saveLoginData.visibility = View.GONE
            } else {
                primary_button.text = getString(R.string.action_sign_in)
                secondary_button.text = getString(R.string.register_new_account)
                showRegisterFields(View.GONE)
                title_login.text = getString(R.string.action_sign_in)
                clearInputs()
                saveLoginData.visibility = View.VISIBLE
            }
            loginFormIsActive = !loginFormIsActive
        }
    }

    private fun clearInputs() {
        login.setText("")
        password.setText("")
        match_password.setText("")
        email.setText("")
        name.setText("")
        surname.setText("")
    }

    private fun formInit() {
        val loginData = SharedPreferenceUtil.getLoginData(applicationContext)
        login.setText(loginData.substringBefore(":"))
        password.setText(loginData.substringAfter(":"))
        saveLoginData.isChecked = loginData.substringBefore(":") != ""
    }

    private fun showRegisterFields(visibility: Int) {
        match_password.visibility = visibility
        email.visibility = visibility
        name.visibility = visibility
        surname.visibility = visibility
    }

    private fun tryLogIn(token: String?) {
        if (token != null) {
            val call = userRepository.getProfile(token)
            call.enqueue(object : Callback<User> {
                override fun onResponse(call: Call<User>?, response: Response<User>?) {
                    if (response != null && response.code() == 200) {
                        val intent = Intent(this@LoginActivity, MainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                        finish()
                    } else {
                        TokenUtil.logout(applicationContext)
                    }
                }

                override fun onFailure(call: Call<User>?, t: Throwable?) {
                    Snackbar.make(
                        findViewById(R.id.login_form), getString(R.string.error_internet_connection), Snackbar.LENGTH_LONG
                    ).setAction("Action", null).show()
                }
            })
        }
    }

    private fun attemptLogin() {
        if (mAuthTask != null)
            return

        val phoneStr = login.text.toString()
        val passwordStr = password.text.toString()
        var cancel = false

        if (TextUtils.isEmpty(phoneStr) || !isPhoneValid(phoneStr))
            cancel = setError(login, getString(R.string.error_incorrect_value))

        if (TextUtils.isEmpty(passwordStr) || !isPasswordValid(passwordStr))
            cancel = setError(password, getString(R.string.error_incorrect_password))

        if (!cancel) {
            showProgress(true)
            mAuthTask = UserLoginTask(LoginUser(phoneStr, passwordStr))
            mAuthTask!!.execute(null as Void?)
        }
    }

    private fun setError(editText: EditText, strOrRes: Any) : Boolean{
        if (strOrRes is Int) {
            editText.error = editText.context.getString(strOrRes)
        } else {
            editText.error = strOrRes as String
        }
        return true
    }

    private fun attemptRegister() {
        if (mRegisterTask != null)
            return

        val phoneStr = login.text.toString()
        val passwordStr = password.text.toString()
        val matchPasswordStr = match_password.text.toString()
        val emailStr = email.text.toString()
        val nameStr = name.text.toString()
        val surnameStr = surname.text.toString()

        if (isFieldsValid(phoneStr, passwordStr, matchPasswordStr, emailStr, nameStr, surnameStr)) {
            showProgress(true)
            mRegisterTask = UserRegisterTask(UserRegistration(passwordStr,emailStr,nameStr,matchPasswordStr,passwordStr,phoneStr,surnameStr))
            mRegisterTask!!.execute(null as Void?)
        }
    }

    private fun isFieldsValid(phoneStr:String, passwordStr:String, matchPasswordStr:String, emailStr:String, nameStr:String, surnameStr:String): Boolean {
        var anyIncorrect = false

        if (TextUtils.isEmpty(phoneStr)|| !isPhoneValid(phoneStr))
            anyIncorrect = setError(login, getString(R.string.error_incorrect_value))

        if (TextUtils.isEmpty(passwordStr))
            anyIncorrect = setError(password, getString(R.string.error_incorrect_value))
        else if (!isPasswordSame(passwordStr, matchPasswordStr))
            anyIncorrect = setError(match_password, getString(R.string.error_password_not_same))

        if (TextUtils.isEmpty(emailStr) || !isEmailValid(emailStr))
            anyIncorrect = setError(email, getString(R.string.error_incorrect_value))

        if (TextUtils.isEmpty(nameStr))
            anyIncorrect = setError(name, getString(R.string.error_field_required))

        if (TextUtils.isEmpty(surnameStr))
            anyIncorrect = setError(surname, getString(R.string.error_field_required))

        return !anyIncorrect
    }

    private fun isEmailValid(email: String): Boolean {
        return email.contains("@") && email.contains(".")
    }

    private fun isPhoneValid(phone: String): Boolean {
        return phone.length == 12
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length > 3
    }

    private fun isPasswordSame(password: String, matchPassword: String): Boolean {
        return password == matchPassword
    }

    private fun showProgress(show: Boolean) {
        val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime).toLong()

        login_form.visibility = if (show) View.GONE else View.VISIBLE
        login_form.animate()
            .setDuration(shortAnimTime)
            .alpha((if (show) 0 else 1).toFloat())
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    login_form.visibility = if (show) View.GONE else View.VISIBLE
                }
            })

        login_progress.visibility = if (show) View.VISIBLE else View.GONE
        login_progress.animate()
            .setDuration(shortAnimTime)
            .alpha((if (show) 1 else 0).toFloat())
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    login_progress.visibility = if (show) View.VISIBLE else View.GONE
                }
            })
    }


    inner class UserLoginTask internal constructor(private val loginUser: LoginUser) :
        AsyncTask<Void, Void, Boolean>() {
        private var errorMsg: String = ""
        override fun doInBackground(vararg params: Void): Boolean? {
            val save = saveLoginData.isChecked

            val call = authRepository.login(loginUser)
            SharedPreferenceUtil.setLoginData(applicationContext, loginUser.phone, loginUser.password, save)
            try {
                val response = call.execute()
                return if (response.isSuccessful) {
                    TokenUtil.setToken(applicationContext, response.body()!!.token)

                    val intent = Intent(this@LoginActivity, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                    true
                } else {
                    errorMsg = getString(R.string.error_incorrect_password)
                    false
                }
            } catch (e: InterruptedException) {
                Log.d("InterruptedException", e.toString())
            } catch (e: IOException) {
                Snackbar.make(
                    findViewById(R.id.login_form),getString(R.string.error_internet_connection),Snackbar.LENGTH_LONG
                ).setAction("Action", null).show()
            }
            return false
        }

        override fun onPostExecute(success: Boolean?) {
            mAuthTask = null
            showProgress(false)

            if (success!!) {
                finish()
            } else if (errorMsg.isNotEmpty()) {
                    password.error = errorMsg
                    password.requestFocus()
                    }
            errorMsg = ""
        }

        override fun onCancelled() {
            mAuthTask = null
            showProgress(false)
        }
    }

    inner class UserRegisterTask internal constructor(private val userRegistration: UserRegistration ) :
        AsyncTask<Void, Void, Boolean>() {
        private var errorMsg: String = ""

        override fun doInBackground(vararg params: Void?): Boolean {
            val call = authRepository.register(userRegistration)
            try {
                val response = call.execute()
                return if (response.isSuccessful) {
                    Snackbar.make(
                        findViewById(R.id.login_form), R.string.registration_success, Snackbar.LENGTH_LONG
                    ).setAction("Action", null).show()
                    true
                } else {
                    Snackbar.make(
                            findViewById(R.id.login_form), R.string.user_exists, Snackbar.LENGTH_LONG
                        ).setAction("Action", null).show()
                    false
                }
            } catch (e: InterruptedException) {
                Log.d("InterruptedException", e.toString())
            } catch (e: IOException) {
                Snackbar.make(
                    findViewById(R.id.login_form),getString(R.string.error_internet_connection),Snackbar.LENGTH_LONG
                ).setAction("Action", null).show()
            }
            return false
        }

        override fun onPostExecute(success: Boolean?) {
            mRegisterTask = null
            showProgress(false)
            val loginUser = LoginUser(userRegistration.phone.toString(), userRegistration.password.toString())

            if (success!!) {
                showProgress(true)
                mAuthTask = UserLoginTask(loginUser)
                mAuthTask!!.execute(null as Void?)
            } else {
                if (errorMsg.isNotEmpty()) {
                    Snackbar.make(findViewById(R.id.login_form), errorMsg, Snackbar.LENGTH_LONG
                    ).setAction("Action", null).show()
                }
            }
            errorMsg = ""
        }

        override fun onCancelled() {
            mRegisterTask = null
            showProgress(false)
        }
    }
}
