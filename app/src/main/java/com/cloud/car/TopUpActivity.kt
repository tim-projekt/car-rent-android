package com.cloud.car

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.cloud.car.models.user.MoneyTransfer
import com.cloud.car.repositories.RepositoryProvider
import com.cloud.car.repositories.UserRepository
import com.cloud.car.utils.TokenUtil
import kotlinx.android.synthetic.main.activity_top_up.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.view.inputmethod.InputMethodManager


class TopUpActivity : AppCompatActivity() {
    private var userRepository: UserRepository = RepositoryProvider.provideUserRepository()

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_top_up)

        initButtonListener()

        val layout: LinearLayout = this@TopUpActivity.findViewById(R.id.mainTopUpLinearLayout)
        layout.setOnTouchListener { view, ev ->
            hideKeyboard(view)
            false
        }
    }

    private fun initButtonListener() {
        topUpNow.setOnClickListener {
            if (amount.text.isNotBlank() && amount.text.isNotEmpty()) {
                createPayment(amount.text.toString().toDouble())
            }
        }
    }

    private fun createPayment(amount: Double) {
        val token = TokenUtil.getToken(this.applicationContext!!)
        val call = userRepository.topUp(token!!, amount)
        call.enqueue(object : Callback<MoneyTransfer> {
            override fun onResponse(call: Call<MoneyTransfer>?, response: Response<MoneyTransfer>?) {
                if (response!!.isSuccessful) {
                    Toast.makeText(
                        this@TopUpActivity,
                        "Successfully added $amount PLN to your account",
                        Toast.LENGTH_SHORT
                    ).show()
                    finish()
                }
            }

            override fun onFailure(call: Call<MoneyTransfer>?, t: Throwable?) {
                finish()
            }
        })
    }

    private fun hideKeyboard(view: View) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }
}
