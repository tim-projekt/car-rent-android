package com.cloud.car

import com.cloud.car.constants.Constant
import com.cloud.car.constants.Mode
import org.junit.Assert
import org.junit.Test

class ConstantUnitTests {

    @Test
    fun serverPortShouldStartWithHttp() {
        Assert.assertTrue(Constant.getServerUrl().startsWith("http"))
    }

    @Test
    fun serverUrlShouldNotBeEmpty() {
        val serverUrl = Constant.getServerUrl()
        Assert.assertTrue(serverUrl.isNotEmpty())
    }

    @Test
    fun urlOnServerAndLocalShouldNotBeTheSame() {
        Constant.mode = Mode.LOCAL
        val localUrl = Constant.getServerUrl()
        Assert.assertTrue(localUrl.isNotEmpty())

        Constant.mode = Mode.SERVER
        val serverUrl = Constant.getServerUrl()
        Assert.assertTrue(serverUrl.isNotEmpty())

        Assert.assertNotEquals(localUrl, serverUrl)
    }
}
