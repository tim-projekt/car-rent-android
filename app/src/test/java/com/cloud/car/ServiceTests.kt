package com.cloud.car

import com.cloud.car.models.user.LoginUser
import com.cloud.car.service.ServerApiService
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test


class ServiceTests {

    private val apiService = ServerApiService.create()
    private val mockUser = LoginUser("+48000000000", "admin")

    @Test
    fun testLogin() {
        val call = apiService.login(mockUser)
        val result = call.execute()
        assertNotNull(result)
        assertEquals(result.code(), 200)
        assertNotNull(result.body())
    }

    @Test
    fun testTariffs() {
        val call = apiService.getTariffs()
        assertNotNull(call)
        val result = call.execute()
        assertEquals(result.code(), 200)
        assertNotNull(result.body())
    }

    @Test
    fun testCars() {
        val call = apiService.getCars()
        val result = call.execute()
        assertNotNull(result)
        assertEquals(result.code(), 200)
        assertNotNull(result.body())
    }

}